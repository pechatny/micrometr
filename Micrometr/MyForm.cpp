#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include "MyForm.h"
using namespace Micrometr;
[STAThreadAttribute]
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew MyForm);
	return 0;
}
