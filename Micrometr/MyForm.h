#pragma once
#using <System.dll>
#using <System.Windows.Forms.dll>
#using <System.Data.dll>
#using <System.Drawing.dll>

//OpenCV Libraries
#include <cv.h>
#include <highgui.h>
#include <stdlib.h>
#include <stdio.h>
//#include "stdafx.h" 
#include <math.h>
namespace Micrometr {

	using namespace System;
	//using namespace System::IO;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Button^  loadButton;
	protected:

	private: System::Windows::Forms::Button^  analisButton;


	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  ����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
	private: System::Windows::Forms::Label^  label2;


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->loadButton = (gcnew System::Windows::Forms::Button());
			this->analisButton = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->pictureBox1->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.BackgroundImage")));
			this->pictureBox1->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->pictureBox1->Location = System::Drawing::Point(12, 40);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(482, 274);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// loadButton
			// 
			this->loadButton->Location = System::Drawing::Point(338, 320);
			this->loadButton->Name = L"loadButton";
			this->loadButton->Size = System::Drawing::Size(75, 20);
			this->loadButton->TabIndex = 1;
			this->loadButton->Text = L"���������";
			this->loadButton->UseVisualStyleBackColor = true;
			this->loadButton->Click += gcnew System::EventHandler(this, &MyForm::loadButton_Click);
			// 
			// analisButton
			// 
			this->analisButton->Location = System::Drawing::Point(419, 320);
			this->analisButton->Name = L"analisButton";
			this->analisButton->Size = System::Drawing::Size(75, 20);
			this->analisButton->TabIndex = 2;
			this->analisButton->Text = L"������";
			this->analisButton->UseVisualStyleBackColor = true;
			this->analisButton->Click += gcnew System::EventHandler(this, &MyForm::analisButton_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 320);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(322, 20);
			this->textBox1->TabIndex = 3;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 353);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(130, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"��������� �����������";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->����ToolStripMenuItem,
					this->toolStripMenuItem1
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(509, 24);
			this->menuStrip1->TabIndex = 5;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// ����ToolStripMenuItem
			// 
			this->����ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->������ToolStripMenuItem,
					this->���������ToolStripMenuItem, this->������������ToolStripMenuItem
			});
			this->����ToolStripMenuItem->Name = L"����ToolStripMenuItem";
			this->����ToolStripMenuItem->Size = System::Drawing::Size(48, 20);
			this->����ToolStripMenuItem->Text = L"����";
			// 
			// ������ToolStripMenuItem
			// 
			this->������ToolStripMenuItem->Name = L"������ToolStripMenuItem";
			this->������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->������ToolStripMenuItem->Text = L"������";
			this->������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::������ToolStripMenuItem_Click);
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->���������ToolStripMenuItem->Text = L"���������";
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->������������ToolStripMenuItem->Text = L"��������� ���...";
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->����������ToolStripMenuItem });
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(65, 20);
			this->toolStripMenuItem1->Text = L"�������";
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(151, 22);
			this->����������ToolStripMenuItem->Text = L"� ���������";
			this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::����������ToolStripMenuItem_Click);
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->Location = System::Drawing::Point(0, 0);
			this->numericUpDown1->Name = L"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(120, 20);
			this->numericUpDown1->TabIndex = 0;
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->Location = System::Drawing::Point(0, 0);
			this->numericUpDown2->Name = L"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(120, 20);
			this->numericUpDown2->TabIndex = 0;
			// 
			// label2
			// 
			this->label2->Location = System::Drawing::Point(0, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(100, 23);
			this->label2->TabIndex = 0;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->ClientSize = System::Drawing::Size(509, 494);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->analisButton);
			this->Controls->Add(this->loadButton);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->menuStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	

	//Load Picture buttons
	private: System::Void loadButton_Click(System::Object^  sender, System::EventArgs^  e) {
				loadPicture();
	}
	private: System::Void ������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				loadPicture();
	}
	
	private: void loadPicture() {
				openFileDialog1->Filter = "JPEG files (*.jpeg)|*.jpeg|JPG files (*.jpg)|*.jpg|All files (*.*)|*.*";
				openFileDialog1->FilterIndex = 2;
				if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				{
					textBox1->Text = openFileDialog1->FileName;
					pictureBox1->Load(openFileDialog1->FileName);
					label1->Text = "�������� ���������";
				}
	}
	//About
	private: System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 MessageBox::Show(
					 "����� �������:\r\n������� ����������\r\n\r\nAuthor:\r\nDmitry Pechatnikov",
					 "", 
					 MessageBoxButtons::OK,
					 MessageBoxIcon::None
				);
	}
	
	//Processing Picture
	private: System::Void analisButton_Click(System::Object^  sender, System::EventArgs^  e) {
				IplImage* image = 0;
				IplImage* gray = 0;
				IplImage* bin = 0;
				IplImage* dst = 0;
				IplImage* adThreshHolded = 0;
				IplImage* threshHolded = 0;
				
				// ��� �������� ������� ������ ����������
				char* filename = convFileName(textBox1->Text);
				// �������� ��������
				image = cvLoadImage(filename, 1);

				////���������� ��� �������
				//IplImage* h_plane = cvCreateImage(cvGetSize(image), 8, 1);
				//IplImage* s_plane = cvCreateImage(cvGetSize(image), 8, 1);
				//IplImage* v_plane = cvCreateImage(cvGetSize(image), 8, 1);

				////����� ����� ��������������
				//IplImage* h_range = 0;
				//IplImage* s_range = 0;
				//IplImage* v_range = 0;

				//// ��� �������� ��������� ��������
				//IplImage* hsv_and = 0;


				//IplImage* hsv = cvCreateImage(cvGetSize(image), 8, 3);

				//cvCvtColor(image, hsv, CV_BGR2HSV);
				//
				//h_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//s_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//v_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//h_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//s_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//v_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				//hsv_and = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);

				//cvCvtPixToPlane(hsv, h_plane, s_plane, v_plane, 0);
				//
				//cvInRangeS(h_plane, cvScalar(0), cvScalar(255), h_range);
				//cvInRangeS(s_plane, cvScalar(0), cvScalar(45), s_range);
				//cvInRangeS(v_plane, cvScalar(0), cvScalar(210), v_range);
				////// ������ ������������� ��������
				////
				////// ���������� 
				////cvAnd(v_range, s_range, hsv_and);
				//cvXor(v_range, s_range, hsv_and);

				//cvAnd(h_range, s_range, hsv_and);
				//cvAnd(hsv_and, v_range, hsv_and);

				threshHolded = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				adThreshHolded = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				gray = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				bin = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
				// ���������
				dst = cvCloneImage(image);
				// ���� ��� ����������� ��������
				/*cvNamedWindow("original", CV_WINDOW_AUTOSIZE);
				cvNamedWindow("binary", CV_WINDOW_AUTOSIZE);*/
				//cvNamedWindow("contours", CV_WINDOW_AUTOSIZE);

				// ����������� � �������� ������
				cvCvtColor(image, gray, CV_RGB2GRAY);
				// ����������� � ��������
				//cvAdaptiveThreshold(threshHolded, threshHolded, 127, 255, CV_THRESH_BINARY);
				//cvAdaptiveThreshold(s_range, threshHolded, 210, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 5, 30);
				//int start = (int) this->numericUpDown1->Value;
				//int end = (int) this->numericUpDown2->Value;
				cvThreshold(gray, threshHolded, 200, 210, CV_THRESH_BINARY);
				cvInRangeS(gray, cvScalar(40), cvScalar(150), bin); // atoi(argv[2])

				CvMemStorage* storage = cvCreateMemStorage(0);
				CvSeq* contours = 0;

				// ������� �������
				int contoursCont = cvFindContours(threshHolded, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
				this->makeOtlet(contours);
				// �������� �������
				for (CvSeq* seq0 = contours; seq0 != 0; seq0 = seq0->h_next){
					cvDrawContours(dst, seq0, CV_RGB(155, 216, 0), CV_RGB(0, 0, 250), 0, 5, 8); // ������ ������
				}
				showPicture(dst);
				// ���������� ��������
				
				//cvShowImage("original", image);
				/*cvShowImage("binary", bin);
				cvShowImage("contours", dst);
				cvShowImage("gray", gray); 
				cvShowImage("threshHolded", threshHolded);*/
				//cvShowImage("adthreshHolded", adThreshHolded);
				//cvShowImage("srange", s_range);
				//cvShowImage("v_srange", v_range);
				//cvShowImage("hsv_and", hsv_and);
				
				// ��� ������� �������
				//cvWaitKey(0);

				// ����������� �������
				cvReleaseImage(&image);
				cvReleaseImage(&gray);
				cvReleaseImage(&bin);
				cvReleaseImage(&dst);
				// ������� ����
				cvDestroyAllWindows();
				//return 0;		 			 	
	}

			 //Show Picture in PictureBox
	private:void showPicture(IplImage *image){
				cvSaveImage("tmp.jpg", image);
				pictureBox1->Load("tmp.jpg");
				remove("tmp.jpg");
	}
			//��������� �������� �������� � ������� �������
	private:void makeOtlet(CvSeq* contours)
	{
				this->label1->Text = "";
				int counter = 1;
				for (CvSeq* seq0 = contours; seq0 != 0; seq0 = seq0->h_next)
				{
					double perim = cvContourPerimeter(seq0);
					double radius = perim / (2 * 3.14159265358979323846);
					//radius = ceil(radius);
					if (perim > 1)
					{

						this->label1->Text = this->label1->Text + "������ �" + counter + "�����: " + radius + "\n";
						this->label1->Text = "\n" + this->label1->Text + "�������� �" + counter + "�����: " + perim + "\n";
						counter++;
					}
					
				}
	}
	//convert FileName from String^ to char*
	private:char* convFileName(String^ name){
				char* filename = (char*)(void*)Marshal::StringToHGlobalAnsi(name);
				return filename;
	}

};
}
